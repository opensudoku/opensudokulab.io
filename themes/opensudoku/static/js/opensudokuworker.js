/*
 * opensudokuworker.js
 * Copyright (C) 2017-2022 Óscar García Amor <ogarcia@connectical.com>
 *
 * Distributed under terms of the GNU GPLv3 license.
 */

importScripts('sudoku.js');

onmessage = function(msg) {
  var puzzle = sudoku.generate(msg.data.level);
  var serialized = sudoku.serialize(puzzle);
  postMessage(serialized);
};
