---
title: "Privacy policy"
subtitle: "Open Sudoku privacy policy"
date: 2024-08-15T09:30:00+02:00
draft: false
---

Open Sudoku authors built the app as an Open Source application. This
SERVICE is provided at no cost and is intended for use as is.
This page is used to inform visitors regarding policies with the collection,
use, and disclosure of Personal Information if anyone decided to use the
Service.
There is no Personal Information that the app collect.
The terms used in this Privacy Policy have the same meanings as in our Terms
and Conditions, which are accessible at Open Sudoku unless otherwise defined
in this Privacy Policy.

### Information collection and use

There is no personally identifiable information collected.
The app does not use any third-party services that may collect information
used to identify you.

### Security

We value your trust in providing us your Personal Information, thus we are
striving to use commercially acceptable means of protecting it. But remember
that no method of transmission over the internet, or method of electronic
storage is 100% secure and reliable, and we cannot guarantee its absolute
security.

### Links to other sites

This Service contains links to other sites. If you click on a third-party
link, you will be directed to that site. Note that these external sites are
not operated by us. Therefore, we strongly advise you to review the Privacy
Policy of these websites. We have no control over and assume no
responsibility for the content, privacy policies, or practices of any
third-party sites or services.

### Children's privacy

This app does not knowingly collect or store personally identifiable
information from anyone.

### Changes to this privacy policy

We may update our Privacy Policy. Thus, you are advised to review this page
periodically for any changes. We will notify you of any changes by posting
the new Privacy Policy on this page.
This policy is effective as of 2024-08-15.

### Contact us

If you have any questions or suggestions about my Privacy Policy, do not
hesitate to [contact us][contact].

[contact]: https://gitlab.com/opensudoku/opensudoku/-/issues
