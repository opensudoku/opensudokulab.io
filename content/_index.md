---
title: "Open Sudoku"
subtitle: "Open Source Sudoku game for Android"
date: 2022-07-24T11:20:00+01:00
draft: false
---

Open Sudoku is a simple open source sudoku game. It’s designed to be
controlled by either finger or keyboard. It’s preloaded with 90 puzzles in
3 difficulty levels, with more puzzles downloadable from the web. It also
allows you to enter your own puzzles.

You can install Open Sudoku via [F-Droid][fdroid], [Google Play][gplay] or
by downloading the latest APK from [GitLab][gitlab]. Note that the three
versions (F-Droid, Google Play, and the APKs) are not compatible (they are
not signed by the same key)! You must uninstall one to install the other,
which will erase all your data.

## About Puzzles

Open Sudoku can import sudoku files downloaded from the web. Just click on
any link below and it will download a file with puzzles that you can then
import into the Open Sudoku application (using the import option from the
_Sudoku list_ menu).

### Base Puzzles

* {{< download href="/opensudoku/easy.opensudoku" >}}Easy{{< /download >}} (100 puzzles)
* {{< download href="/opensudoku/medium.opensudoku" >}}Medium{{< /download >}} (100 puzzles)
* {{< download href="/opensudoku/hard.opensudoku" >}}Hard{{< /download >}} (100 puzzles)
* {{< download href="/opensudoku/very_hard.opensudoku" >}}Very Hard{{< /download >}} (100 puzzles)

### Gnome Sudoku Puzzles

* Set One
  * {{< download href="/opensudoku/gs1_easy.opensudoku" >}}Easy{{< /download >}} (100 puzzles)
  * {{< download href="/opensudoku/gs1_medium.opensudoku" >}}Medium{{< /download >}} (100 puzzles)
  * {{< download href="/opensudoku/gs1_hard.opensudoku" >}}Hard{{< /download >}} (100 puzzles)
  * {{< download href="/opensudoku/gs1_very_hard.opensudoku" >}}Very Hard{{< /download >}} (100 puzzles)
* Set Two
  * {{< download href="/opensudoku/gs2_easy.opensudoku" >}}Easy{{< /download >}} (100 puzzles)
  * {{< download href="/opensudoku/gs2_medium.opensudoku" >}}Medium{{< /download >}} (100 puzzles)
  * {{< download href="/opensudoku/gs2_hard.opensudoku" >}}Hard{{< /download >}} (100 puzzles)
  * {{< download href="/opensudoku/gs2_very_hard.opensudoku" >}}Very Hard{{< /download >}} (100 puzzles)
* Set Three
  * {{< download href="/opensudoku/gs3_easy.opensudoku" >}}Easy{{< /download >}} (100 puzzles)
  * {{< download href="/opensudoku/gs3_medium.opensudoku" >}}Medium{{< /download >}} (100 puzzles)
  * {{< download href="/opensudoku/gs3_hard.opensudoku" >}}Hard{{< /download >}} (100 puzzles)
  * {{< download href="/opensudoku/gs3_very_hard.opensudoku" >}}Very Hard{{< /download >}} (100 puzzles)

### QQWing Puzzles

The following puzzles are generated with [QQWing][qqwing]. You can generate
new puzzles by downloading the [QQWing binary][qqwingdownload] and executing
one of the following commands from a POSIX console.
```sh
# Very easy level
qqwing --generate 1000 --difficulty simple --one-line | \
  tr '.' '0' > qqwing_simple.sdm

# Easy level
qqwing --generate 1000 --difficulty easy --one-line | \
  tr '.' '0' > qqwing_easy.sdm

# Medium level
qqwing --generate 1000 --difficulty intermediate --one-line | \
  tr '.' '0' > qqwing_intermediate.sdm

# Expert level
qqwing --generate 1000 --difficulty expert --one-line | \
  tr '.' '0' > qqwing_expert.sdm
```

* {{< download href="/sdm/qqwing_simple.sdm" >}}Simple{{< /download >}} (1000 puzzles)
* {{< download href="/sdm/qqwing_easy.sdm" >}}Easy{{< /download >}} (1000 puzzles)
* {{< download href="/sdm/qqwing_intermediate.sdm" >}}Intermediate{{< /download >}} (1000 puzzles)
* {{< download href="/sdm/qqwing_expert.sdm" >}}Expert{{< /download >}} (1000 puzzles)

### SudoCue Puzzles

Puzzles from [SudoCue][sudocuedownload]. On [their website][sudocue] you
can find daily challenges and other Sudoku variants.

* {{< download href="/sdm/sudocue_snow2005.sdm" >}}100 Snowflake Puzzles{{< /download >}} (100 puzzles)
* {{< download href="/sdm/sudocue_superiors.sdm" >}}The Superiors Collection{{< /download >}} (160 puzzles)
* {{< download href="/sdm/sudocue_noponies.sdm" >}}The No-Trick Ponies Collection{{< /download >}} (250 puzzles)
* {{< download href="/sdm/sudocue_almostdn.sdm" >}}The Almost Daily Nightmare Collection{{< /download >}} (400 puzzles)
* {{< download href="/sdm/sudocue_learningcurve.sdm" >}}The Learning Curve Collection{{< /download >}} (2500 puzzles)
* {{< download href="/sdm/sudocue_top10000.sdm" >}}Top 10000 Sudokus{{< /download >}} (10000 puzzles)
* {{< download href="/sdm/sudocue_top50000.sdm" >}}50K Tabling Puzzles{{< /download >}} (50000 puzzles) 🤪

### Generated Puzzles

If you want more puzzles, select how many you want and the difficulty level
(the higher the number the longer it will take). New puzzles will be
generated each time you click on generate.

{{< generator >}}

## Authors and Contributors

Open Sudoku since version 4 is authored by [Bart Uliasz][buliasz].

Previous versions of Open Sudoku were authored by [Óscar García
Amor][ogarcia].

And was initially developed by [Roman Mašek][roman] with contributions from
Vit Hnilica, Martin Sobola, Martin Helff and Diego Pierotto.

You can see the complete list of contributors [here][contributors].

## Help with translation

Interested in help to translate Open Sudoku? You can contribute in our
[Weblate team][translation].

## Support and contact

Having trouble with Open Sudoku? Fill an issue in [our issue
tracker][issues].

To chat with us you can do it in our [Matrix channel][matrix].

[fdroid]: https://f-droid.org/en/packages/org.moire.opensudoku/
[gplay]: https://play.google.com/store/apps/details?id=org.moire.opensudoku
[gitlab]: https://gitlab.com/opensudoku/opensudoku/-/releases
[qqwing]: https://qqwing.com/
[qqwingdownload]: https://qqwing.com/download.html
[sudocue]: http://www.sudocue.net/
[sudocuedownload]: http://www.sudocue.net/download.php
[buliasz]: https://github.com/buliasz
[ogarcia]: https://ogarcia.me/
[roman]: https://github.com/romario333/opensudoku
[contributors]: https://gitlab.com/opensudoku/opensudoku/-/graphs/develop
[translation]: https://hosted.weblate.org/projects/opensudoku/
[issues]: https://gitlab.com/opensudoku/opensudoku/issues
[matrix]: https://matrix.to/#/#opensudoku_opensudoku:gitter.im
